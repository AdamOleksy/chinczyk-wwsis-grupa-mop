package application;

import java.util.Random;

import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;

public class Kostka {
	
	public int rzutKostka(){
		Random generator = new Random();	
		return generator.nextInt(6) + 1;
	}

}
