package application;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(this.getClass().getResource("Plansza.fxml"));	
		//PlanszaController controller = loader.getController();
		
		
		StackPane stackPane = loader.load();
		//loader.setController(controller); //wpliku fxml juz jest podane 
		Scene scene = new Scene(stackPane);
		primaryStage.setScene(scene);
		primaryStage.setTitle("chinczyk");
		primaryStage.show();
		//primaryStage.setFullScreen(true);
	}
	
	public static void main(String[] args) {
		
		//ListaGraczy lista = new ListaGraczy();
		
		launch(args);
		
	}
}
