package application;

import javafx.animation.FadeTransition;
import javafx.animation.FillTransition;
import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;


public class PlanszaController {
	
	@FXML
	private Circle bitypionekHologram;
	@FXML
	private Circle r;
	@FXML
	private Label label01;
	@FXML
	private Label label02;
	@FXML
	private Button przycisk;
	@FXML
	private Button przycisk2;
	@FXML
	private Rectangle r124;
	@FXML
	private Rectangle r123;
	@FXML
	private Rectangle r122;
	@FXML
	private Rectangle r121;
	@FXML
	private Rectangle r144;
	@FXML
	private Rectangle r143;
	@FXML
	private Rectangle r142;
	@FXML
	private Rectangle r141;
	@FXML
	private Rectangle r134;
	@FXML
	private Rectangle r133;
	@FXML
	private Rectangle r132;
	@FXML
	private Rectangle r131;
	@FXML
	private Rectangle r114;
	@FXML
	private Rectangle r113;
	@FXML
	private Rectangle r112;
	@FXML
	private Rectangle r111;
	@FXML
	private Rectangle r21;
	@FXML
	private Rectangle r20;
	@FXML
	private Rectangle r19;
	@FXML
	private Rectangle r15;
	@FXML
	private Rectangle r11;
	@FXML
	private Rectangle r10;
	@FXML
	private Rectangle r09;
	@FXML
	private Rectangle r05;
	@FXML
	private Rectangle r01;
	@FXML
	private Rectangle r40;
	@FXML
	private Rectangle r39;
	@FXML
	private Rectangle r35;
	@FXML
	private Rectangle r31;
	@FXML
	private Rectangle r30;
	@FXML
	private Rectangle r29;
	@FXML
	private Rectangle r25;
	@FXML
	private Rectangle r18;
	@FXML
	private Rectangle r17;
	@FXML
	private Rectangle r16;
	@FXML
	private Rectangle r22;
	@FXML
	private Rectangle r23;
	@FXML
	private Rectangle r24;
	@FXML
	private Rectangle r02;
	@FXML
	private Rectangle r03;
	@FXML
	private Rectangle r04;
	@FXML
	private Rectangle r38;
	@FXML
	private Rectangle r37;
	@FXML
	private Rectangle r36;
	@FXML
	private Rectangle r06;
	@FXML
	private Rectangle r07;
	@FXML
	private Rectangle r08;
	@FXML
	private Rectangle r34;
	@FXML
	private Rectangle r33;
	@FXML
	private Rectangle r32;
	@FXML
	private Rectangle r14;
	@FXML
	private Rectangle r13;
	@FXML
	private Rectangle r12;
	@FXML
	private Rectangle r26;
	@FXML
	private Rectangle r27;
	@FXML
	private Rectangle r28;
	@FXML
	public Circle r203;
	@FXML
	private Circle r201;
	@FXML
	private Circle r204;
	@FXML
	private Circle r202;
	@FXML
	private Circle r301;
	@FXML
	private Circle r303;
	@FXML
	private Circle r304;
	@FXML
	private Circle r302;
	@FXML
	private Circle r402;
	@FXML
	private Circle r404;
	@FXML
	private Circle r401;
	@FXML
	private Circle r403;
	@FXML
	private Circle r104;
	@FXML
	private Circle r102;
	@FXML
	private Circle r101;
	@FXML
	private Circle r103;
	
	
	
	
	
	private int pozycjaPionka101=-11,pozycjaPionka102=-12,pozycjaPionka103=-13,pozycjaPionka104=-14,
			    pozycjaPionka201=-11,pozycjaPionka202=-12,pozycjaPionka203=-13,pozycjaPionka204=-14,
			    pozycjaPionka301=-11,pozycjaPionka302=-12,pozycjaPionka303=-13,pozycjaPionka304=-14,
		        pozycjaPionka401=-11,pozycjaPionka402=-12,pozycjaPionka403=-13,pozycjaPionka404=-14,
		        rk=0,pa,iloscWprowadzonychNazw=0,licznikBlokad;
	private Kostka kostka =new Kostka();
	private RuchPionka ruchpionka = new RuchPionka();
	private Gra gra = new Gra();


	double tabGreen [][] = new double [][] {{200.0,160.0,120.0, 80.0,40.0,40.0, 40.0, 40.0, 40.0,  0.0,-40.0,-40.0,-40.0,-40.0,-40.0,-80.0,-120.0,-160.0,-200.0, -200.0,-200.0,-160.0,-120.0,-80.0, -40.0,-40.0, -40.0, -40.0, -40.0,   0.0,  40.0,  40.0,  40.0, 40.0, 40.0, 80.0,120.0,160.0,200.0,200.0,160.0,120.0,80.0,40.0},
		                                    { 40.0, 40.0, 40.0, 40.0,40.0,80.0,120.0,160.0,200.0,200.0,200.0,160.0,120.0, 80.0, 40.0, 40.0, 40.0,   40.0,  40.0,   0.0,  -40.0, -40.0, -40.0,-40.0, -40.0,-80.0,-120.0,-160.0,-200.0,-200.0,-200.0,-160.0,-120.0,-80.0,-40.0,-40.0,-40.0,-40.0,-40.0,  0.0,  0.0,  0.0, 0.0, 0.0}};
		                                    
    double tabBlue [][] = new double [][] {{ -40.0,-40.0,-40.0,-40.0,-40.0,-80.0,-120.0,-160.0,-200.0, -200.0,-200.0,-160.0,-120.0,-80.0, -40.0,-40.0, -40.0, -40.0, -40.0,   0.0,  40.0,  40.0,  40.0, 40.0, 40.0, 80.0,120.0,160.0,200.0,200.0,200.0,160.0,120.0, 80.0,40.0,40.0, 40.0, 40.0, 40.0, 0.0,  0.0,  0.0, 0.0, 0.0},
			                                {200.0,160.0,120.0, 80.0, 40.0, 40.0, 40.0,   40.0,  40.0,   0.0,  -40.0, -40.0, -40.0,-40.0, -40.0,-80.0,-120.0,-160.0,-200.0,-200.0,-200.0,-160.0,-120.0,-80.0,-40.0,-40.0,-40.0,-40.0,-40.0,  0.0, 40.0, 40.0, 40.0, 40.0,40.0,80.0,120.0,160.0,200.0,200.0,160.0,120.0,80.0,40.0}};
  
	double tabRed [][] = new double [][] {{-200.0,-160.0,-120.0,-80.0, -40.0,-40.0, -40.0, -40.0, -40.0,   0.0,  40.0,  40.0,  40.0, 40.0, 40.0, 80.0,120.0,160.0,200.0,200.0,200.0,160.0,120.0, 80.0,40.0,40.0, 40.0, 40.0, 40.0,  0.0,-40.0,-40.0,-40.0,-40.0,-40.0,-80.0,-120.0,-160.0,-200.0, -200.0,-160.0,-120.0,-80.0,-40.0},
        								   {-40.0, -40.0, -40.0,-40.0, -40.0,-80.0,-120.0,-160.0,-200.0,-200.0,-200.0,-160.0,-120.0,-80.0,-40.0,-40.0,-40.0,-40.0,-40.0,  0.0, 40.0, 40.0, 40.0, 40.0,40.0,80.0,120.0,160.0,200.0,200.0,200.0,160.0,120.0, 80.0, 40.0, 40.0, 40.0,   40.0,  40.0,    0.0,   0.0,   0.0,  0.0,  0.0}};

	double tabYellow [][] = new double [][] {{ 40.0,  40.0,  40.0, 40.0, 40.0, 80.0,120.0,160.0,200.0,200.0,200.0,160.0,120.0, 80.0,40.0,40.0, 40.0, 40.0, 40.0, 0.0,-40.0,-40.0,-40.0,-40.0,-40.0,-80.0,-120.0,-160.0,-200.0, -200.0,-200.0,-160.0,-120.0,-80.0, -40.0,-40.0, -40.0, -40.0, -40.0,   0.0, 0.0,  0.0, 0.0,  0.0},
											{-200.0,-160.0,-120.0,-80.0,-40.0,-40.0,-40.0,-40.0,-40.0,  0.0,40.0,  40.0, 40.0, 40.0,40.0,80.0,120.0,160.0,200.0,200.0,200.0,160.0,120.0, 80.0, 40.0, 40.0, 40.0,   40.0,  40.0,    0.0, -40.0, -40.0, -40.0,-40.0, -40.0,-80.0,-120.0,-160.0,-200.0,-200.0,-160.0,-120.0,-80.0,-40.0}};

	
	public void jestesZwyciezca(){
		if(pozycjaPionka101>39 && pozycjaPionka102>39 && pozycjaPionka103>39 && pozycjaPionka104>39)
			System.out.print("Zielony!!");
		else if(pozycjaPionka201>39 && pozycjaPionka202>39 && pozycjaPionka203>39 && pozycjaPionka204>39)
			System.out.print("Niebieski!!");
		else if(pozycjaPionka301>39 && pozycjaPionka302>39 && pozycjaPionka303>39 && pozycjaPionka304>39)
			System.out.print("Czerwony!!");
		else if(pozycjaPionka401>39 && pozycjaPionka402>39 && pozycjaPionka403>39 && pozycjaPionka404>39)
			System.out.print("Zolty!!");
	}
	public void bicie(){
		if(bitypionekHologram.getTranslateX()==r101.getTranslateX()&&bitypionekHologram.getTranslateY()==r101.getTranslateY()){
			pozycjaPionka101=-11;
			r101.setTranslateX(155);
			r101.setTranslateY(155);
		}
		else if(bitypionekHologram.getTranslateX()==r102.getTranslateX()&&bitypionekHologram.getTranslateY()==r102.getTranslateY()){
			pozycjaPionka102=-12;
			r102.setTranslateX(200);
			r102.setTranslateY(155);
		}
		else if(bitypionekHologram.getTranslateX()==r103.getTranslateX()&&bitypionekHologram.getTranslateY()==r103.getTranslateY()){
			pozycjaPionka103=-13;
			r103.setTranslateX(155);
			r103.setTranslateY(200);
		}
		else if(bitypionekHologram.getTranslateX()==r104.getTranslateX()&&bitypionekHologram.getTranslateY()==r104.getTranslateY()){
			pozycjaPionka104=-14;
			r104.setTranslateX(200);
			r104.setTranslateY(200);
		}
		else if(bitypionekHologram.getTranslateX()==r201.getTranslateX()&&bitypionekHologram.getTranslateY()==r201.getTranslateY()){
			pozycjaPionka201=-11;
			r201.setTranslateX(-200);
			r201.setTranslateY(155);
		}
		else if(bitypionekHologram.getTranslateX()==r202.getTranslateX()&&bitypionekHologram.getTranslateY()==r202.getTranslateY()){
			pozycjaPionka202=-12;
			r202.setTranslateX(-155);
			r202.setTranslateY(155);
		}
		else if(bitypionekHologram.getTranslateX()==r203.getTranslateX()&&bitypionekHologram.getTranslateY()==r203.getTranslateY()){
			pozycjaPionka203=-13;
			r203.setTranslateX(-200);
			r203.setTranslateY(200);
		}
		else if(bitypionekHologram.getTranslateX()==r204.getTranslateX()&&bitypionekHologram.getTranslateY()==r204.getTranslateY()){
			pozycjaPionka204=-14;
			r204.setTranslateX(-155);
			r204.setTranslateY(200);
		}
		else if(bitypionekHologram.getTranslateX()==r301.getTranslateX()&&bitypionekHologram.getTranslateY()==r301.getTranslateY()){
			pozycjaPionka301=-11;
			r301.setTranslateX(-200);
			r301.setTranslateY(-200);
		}
		else if(bitypionekHologram.getTranslateX()==r302.getTranslateX()&&bitypionekHologram.getTranslateY()==r302.getTranslateY()){
			pozycjaPionka302=-12;
			r302.setTranslateX(-155);
			r302.setTranslateY(-200);
		}
		else if(bitypionekHologram.getTranslateX()==r303.getTranslateX()&&bitypionekHologram.getTranslateY()==r303.getTranslateY()){
			pozycjaPionka303=-13;
			r303.setTranslateX(-200);
			r303.setTranslateY(-155);
		}
		else if(bitypionekHologram.getTranslateX()==r304.getTranslateX()&&bitypionekHologram.getTranslateY()==r304.getTranslateY()){
			pozycjaPionka304=-14;
			r304.setTranslateX(-155);
			r304.setTranslateY(-155);
		}
		else if(bitypionekHologram.getTranslateX()==r401.getTranslateX()&&bitypionekHologram.getTranslateY()==r401.getTranslateY()){
			pozycjaPionka401=-11;
			r401.setTranslateX(155);
			r401.setTranslateY(-200);
		}
		if(bitypionekHologram.getTranslateX()==r402.getTranslateX()&&bitypionekHologram.getTranslateY()==r402.getTranslateY()){
			pozycjaPionka402=-12;
			r402.setTranslateX(200);
			r404.setTranslateY(-200);
		}
		else if(bitypionekHologram.getTranslateX()==r403.getTranslateX()&&bitypionekHologram.getTranslateY()==r403.getTranslateY()){
			pozycjaPionka403=-13;
			r403.setTranslateX(155);
			r403.setTranslateY(-155);
		}
		else if(bitypionekHologram.getTranslateX()==r404.getTranslateX()&&bitypionekHologram.getTranslateY()==r404.getTranslateY()){
			pozycjaPionka404=-14;
			r404.setTranslateX(200);
			r404.setTranslateY(-155);
		}
		
		
	}

	public void wykryjBicie(Circle c) {
		bitypionekHologram.setTranslateX(267);
		Circle tabGreenBicie[]   =new Circle []{r201,r202,r203,r204,r301,r302,r303,r304,r401,r402,r403,r404};
		Circle tabBlueBicie[]    =new Circle []{r101,r102,r103,r104,r301,r302,r303,r304,r401,r402,r403,r404};
		Circle tabRedBicie[]     =new Circle []{r101,r102,r103,r104,r201,r202,r203,r204,r401,r402,r403,r404};
		Circle tabYellowBicie[]  =new Circle []{r101,r102,r103,r104,r201,r202,r203,r204,r301,r302,r303,r304};
		if(c.equals(r101)||c.equals(r102)||c.equals(r103)||c.equals(r104)) {
			for(int i=0;i<12;i++){
				if(r.getTranslateX()==tabGreenBicie[i].getTranslateX()&&r.getTranslateY()==tabGreenBicie[i].getTranslateY()){
					label01.setText("Bicie");
					bitypionekHologram.setTranslateX(tabGreenBicie[i].getTranslateX());
					bitypionekHologram.setTranslateY(tabGreenBicie[i].getTranslateY());
				}
			}
		}
		else if(c.equals(r201)||c.equals(r202)||c.equals(r203)||c.equals(r204)){
			for(int i=0;i<12;i++){
				if(r.getTranslateX()==tabBlueBicie[i].getTranslateX()&&r.getTranslateY()==tabBlueBicie[i].getTranslateY()){
					label01.setText("Bicie");
					bitypionekHologram.setTranslateX(tabBlueBicie[i].getTranslateX());
					bitypionekHologram.setTranslateY(tabBlueBicie[i].getTranslateY());
				}
			}
		}
		else if(c.equals(r301)||c.equals(r302)||c.equals(r303)||c.equals(r304)) {
			for(int i=0;i<12;i++){
				if(r.getTranslateX()==tabRedBicie[i].getTranslateX()&&r.getTranslateY()==tabRedBicie[i].getTranslateY()){
					label01.setText("Bicie");
					bitypionekHologram.setTranslateX(tabRedBicie[i].getTranslateX());
					bitypionekHologram.setTranslateY(tabRedBicie[i].getTranslateY());
					
				}
			}
		}
		else if(c.equals(r401)||c.equals(r402)||c.equals(r403)||c.equals(r404)) {
			for(int i=0;i<12;i++){
				if(r.getTranslateX()==tabYellowBicie[i].getTranslateX()&&r.getTranslateY()==tabYellowBicie[i].getTranslateY()){
					label01.setText("Bicie");
					bitypionekHologram.setTranslateX(tabYellowBicie[i].getTranslateX());
					bitypionekHologram.setTranslateY(tabYellowBicie[i].getTranslateY());
				}
			}	
		}
	}
		

	public PlanszaController(){
	
	}
	@SuppressWarnings("unchecked")
	@FXML
	void initialize(){
		
		bitypionekHologram.setCenterX(1);
		bitypionekHologram.toBack();
		bitypionekHologram.setVisible(false);
		nazwaGraczZielony.setDisable(true);
		nazwaGraczNiebieski.setDisable(true);
		nazwaGraczCzerwony.setDisable(true);
		nazwaGraczZolty.setDisable(true);
		widzialnePionkiZielone(false);
		widzialnePionkiNiebieski(false);
		widzialnePionkiCzerwone(false);
		widzialnePionkiZolte(false);
		
		iloscGraczyBox.toFront();
		iloscGraczyBox.getItems().removeAll(iloscGraczyBox.getItems());
		iloscGraczyBox.getItems().addAll("2","3","4");
		iloscGraczyBox.getSelectionModel().select("Ilosc graczy");
		
		przycisk.setText("Start");
		r.setVisible(false);
		blokujWszystko();
		przycisk.setDisable(true);
		przycisk2.setDisable(true);
		przycisk2.toBack();
		
	}
	
	@FXML
	public void klikPrzycisk() {
		rk=kostka.rzutKostka();
		
		Polyline polyLine = new Polyline();
		polyLine.getPoints().addAll(new Double[]{
				45.0, -230.0,
				200.0, 100.0,
				-100.0, 200.0,
				-200.0, -150.0,
				45.0, -230.0
		});
		PathTransition pTransition = new PathTransition(Duration.millis(2000),polyLine, label02);
		pTransition.setAutoReverse(true);
		pTransition.play();
		FadeTransition fTransition = new FadeTransition(Duration.millis(3000), label02);
		fTransition.setFromValue(0.0);
		fTransition.setToValue(1.0);
		fTransition.play();
		RotateTransition rTransition = new RotateTransition(Duration.millis(2000), label02);
		rTransition.setByAngle(7200);
		rTransition.play();
		ScaleTransition sTransitional = new ScaleTransition(Duration.millis(2000), label02);
		sTransitional.setToX(1);
		sTransitional.setToY(1);
		sTransitional.setFromX(3);
		sTransitional.setFromY(3);
		sTransitional.setToX(1);
		sTransitional.setToY(1);
		sTransitional.play();
		
		
		
		label01.setText("wypadlo");
		wypiszNaLable02(String.valueOf(rk));
		
		
		przycisk.setDisable(true);
		
		if(gra.gracz == 0) {
			label01.setText(nazwaGraczZielony.getText()+" wybierz pionek");
			odblokujZielone();
		}
		else if(gra.gracz == 1){
			label01.setText(nazwaGraczNiebieski.getText()+" wybierz pionek");
			odblokujNiebieski();
		}
		else if(gra.gracz == 2){
			label01.setText(nazwaGraczCzerwony.getText()+" wybierz pionek");
			odblokujCzerwony();
		}
		else if(gra.gracz == 3){
			label01.setText(nazwaGraczZolty.getText()+" wybierz pionek");
			odblokujZolty();
		}
		mozliwoscRuchu();
		
	}
	@FXML
	public void klikPrzycisk2(){
		przycisk2.setDisable(true);
		przycisk2.toBack();
		odblokujPrzycisk();
		label01.setText("rzuc koscia");
	}
	@FXML
	public void onMousePrzycisk(){
		przycisk.setText("rzut");
	}
	@FXML
	public void onMousePole(){
		r01.setFill(Color.YELLOW);
	}
	@FXML
	public void outMousePole(){
		r01.setFill(Color.WHITE);
	}
	
	@FXML
    void klikCircler101() {
		  if(pozycjaPionka101<0)pozycjaPionka101=0;
		  else pozycjaPionka101 = ruchpionka.obliczRuchPionka(pozycjaPionka101,rk);
		  wykryjBicie(r101);
		  bicie();
		  r101.setTranslateX(tabGreen[0][pozycjaPionka101]);
		  r101.setTranslateY(tabGreen[1][pozycjaPionka101]);
		  odblokujPrzycisk();
		  blokujZielone();
		  jestesZwyciezca();
		  
	    }

	    @FXML
	    void klikCircler102(MouseEvent event) {
	    	if(pozycjaPionka102<0)pozycjaPionka102=0;
			  else pozycjaPionka102 = ruchpionka.obliczRuchPionka(pozycjaPionka102,rk);
	    	  wykryjBicie(r102);
	    	  bicie();
			  r102.setTranslateX(tabGreen[0][pozycjaPionka102]);
			  r102.setTranslateY(tabGreen[1][pozycjaPionka102]);
			  odblokujPrzycisk();
			  blokujZielone();
			  jestesZwyciezca();
			  

	    }

	    @FXML
	    void klikCircler103(MouseEvent event) {
	    	if(pozycjaPionka103<0)pozycjaPionka103=0;
			  else pozycjaPionka103 = ruchpionka.obliczRuchPionka(pozycjaPionka103,rk);
	    		  wykryjBicie(r103);
		    	  bicie();
		    	  r103.setTranslateX(tabGreen[0][pozycjaPionka103]);
				  r103.setTranslateY(tabGreen[1][pozycjaPionka103]);
				  
				  blokujZielone();
				  odblokujPrzycisk();
				  jestesZwyciezca();
				 
	    }

	    @FXML
	    void klikCircler104(MouseEvent event) {
	    	if(pozycjaPionka104<0)pozycjaPionka104=0;
			  else pozycjaPionka104 = ruchpionka.obliczRuchPionka(pozycjaPionka104,rk);
	    	  wykryjBicie(r104);
	    	  bicie();
			  r104.setTranslateX(tabGreen[0][pozycjaPionka104]);
			  r104.setTranslateY(tabGreen[1][pozycjaPionka104]);
			  
			  blokujZielone();
			  odblokujPrzycisk();
			  jestesZwyciezca();
			 
	    }

	    @FXML
	    void klikCircler201(MouseEvent event) {
	    	if(pozycjaPionka201<0)pozycjaPionka201=0;
			  else pozycjaPionka201 = ruchpionka.obliczRuchPionka(pozycjaPionka201,rk);
	    		wykryjBicie(r201);
	    		bicie();
	    		r201.setTranslateX(tabBlue[0][pozycjaPionka201]);
			  r201.setTranslateY(tabBlue[1][pozycjaPionka201]);  
			  blokujNiebieski();
			  odblokujPrzycisk();
			  jestesZwyciezca();
			 
	    }
	    
	    @FXML
	    void klikCircler202(MouseEvent event) {
	    	if(pozycjaPionka202<0)pozycjaPionka202=0;
			  else pozycjaPionka202 = ruchpionka.obliczRuchPionka(pozycjaPionka202,rk);
    		wykryjBicie(r202);
    		bicie();
			  r202.setTranslateX(tabBlue[0][pozycjaPionka202]);
			  r202.setTranslateY(tabBlue[1][pozycjaPionka202]);
			  blokujNiebieski();
			  odblokujPrzycisk();
			

	    }

	    @FXML
	    void klikCircler203(MouseEvent event) {
	    	if(pozycjaPionka203<0)pozycjaPionka203=0;
			  else pozycjaPionka203 = ruchpionka.obliczRuchPionka(pozycjaPionka203,rk);
    		wykryjBicie(r203);
    		bicie();
	    	r203.setTranslateX(tabBlue[0][pozycjaPionka203]);
			  r203.setTranslateY(tabBlue[1][pozycjaPionka203]);
			  blokujNiebieski();
			  odblokujPrzycisk();
			 

	    }

	    @FXML
	    void klikCircler204(MouseEvent event) {
	    	if(pozycjaPionka204<0)pozycjaPionka204=0;
			  else pozycjaPionka204 = ruchpionka.obliczRuchPionka(pozycjaPionka204,rk);
    		wykryjBicie(r204);
    		bicie();			 
	    	r204.setTranslateX(tabBlue[0][pozycjaPionka204]);
			  r204.setTranslateY(tabBlue[1][pozycjaPionka204]);
			  blokujNiebieski();
			  odblokujPrzycisk();
	    }

	    @FXML
	    void klikCircler301(MouseEvent event) {
	    	if(pozycjaPionka301<0)pozycjaPionka301=0;
			  else pozycjaPionka301 = ruchpionka.obliczRuchPionka(pozycjaPionka301,rk);
    		wykryjBicie(r301);
    		bicie();
	    	r301.setTranslateX(tabRed[0][pozycjaPionka301]);
			  r301.setTranslateY(tabRed[1][pozycjaPionka301]);
			  blokujCzerwony();
			  odblokujPrzycisk();
	    }

	    @FXML
	    void klikCircler302(MouseEvent event) {
	    	if(pozycjaPionka302<0)pozycjaPionka302=0;
			  else pozycjaPionka302 = ruchpionka.obliczRuchPionka(pozycjaPionka302,rk);
    		wykryjBicie(r302);
    		bicie();
	    	r302.setTranslateX(tabRed[0][pozycjaPionka302]);
			  r302.setTranslateY(tabRed[1][pozycjaPionka302]);
			  blokujCzerwony();
			  odblokujPrzycisk();
	    }

	    @FXML
	    void klikCircler303(MouseEvent event) {
	    	if(pozycjaPionka303<0)pozycjaPionka303=0;
			  else pozycjaPionka303 = ruchpionka.obliczRuchPionka(pozycjaPionka303,rk);
    		wykryjBicie(r303);
    		bicie();
	    	r303.setTranslateX(tabRed[0][pozycjaPionka303]);
			  r303.setTranslateY(tabRed[1][pozycjaPionka303]);
			  blokujCzerwony();
			  odblokujPrzycisk();
	    }

	    @FXML
	    void klikCircler304(MouseEvent event) {
	    	if(pozycjaPionka304<0)pozycjaPionka304=0;
			  else pozycjaPionka304 = ruchpionka.obliczRuchPionka(pozycjaPionka304,rk);
    		wykryjBicie(r304);
    		bicie();
	    	r304.setTranslateX(tabRed[0][pozycjaPionka304]);
			  r304.setTranslateY(tabRed[1][pozycjaPionka304]);
			  blokujCzerwony();
			  odblokujPrzycisk();
	    }

	    @FXML
	    void klikCircler401(MouseEvent event) {
	    	if(pozycjaPionka401<0)pozycjaPionka401=0;
			  else pozycjaPionka401 = ruchpionka.obliczRuchPionka(pozycjaPionka401,rk);
    		wykryjBicie(r401);
    		bicie();
	    	r401.setTranslateX(tabYellow[0][pozycjaPionka401]);
			  r401.setTranslateY(tabYellow[1][pozycjaPionka401]);
			  blokujZolty();
			  odblokujPrzycisk();
	    }

	    @FXML
	    void klikCircler402(MouseEvent event) {
	    	if(pozycjaPionka402<0)pozycjaPionka402=0;
			  else pozycjaPionka402 = ruchpionka.obliczRuchPionka(pozycjaPionka402,rk);
	    	wykryjBicie(r402);
    		bicie();
	    	r402.setTranslateX(tabYellow[0][pozycjaPionka402]);
			  r402.setTranslateY(tabYellow[1][pozycjaPionka402]);
			  blokujZolty();
			  odblokujPrzycisk();
		    	
	    }

	    @FXML
	    void klikCircler403(MouseEvent event) {
	    	if(pozycjaPionka403<0)pozycjaPionka403=0;
			  else pozycjaPionka403 = ruchpionka.obliczRuchPionka(pozycjaPionka403,rk);
	    	wykryjBicie(r403);
    		bicie();
	    	r403.setTranslateX(tabYellow[0][pozycjaPionka403]);
			  r403.setTranslateY(tabYellow[1][pozycjaPionka403]);
			  blokujZolty();
			  odblokujPrzycisk();
	    }

	    @FXML
	    void klikCircler404(MouseEvent event) {
	    	if(pozycjaPionka404<0)pozycjaPionka404=0;
			  else pozycjaPionka404 = ruchpionka.obliczRuchPionka(pozycjaPionka404,rk);
	    	wykryjBicie(r404);
    		bicie();
	    	r404.setTranslateX(tabYellow[0][pozycjaPionka404]);
			  r404.setTranslateY(tabYellow[1][pozycjaPionka404]);
			  blokujZolty();
			  odblokujPrzycisk();
	    }

	    @FXML
	    void onMouseCirrcler101(MouseEvent event) {
	    	
	    	  if(pozycjaPionka101<0)pa=0;
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka101,rk);
	    	  if(pa==pozycjaPionka102||pa==pozycjaPionka103||pa==pozycjaPionka104)r101.setDisable(true);
		     else{
	    	  r.setTranslateX(tabGreen[0][pa]);
			  r.setTranslateY(tabGreen[1][pa]);
			  r.setStroke(Color.LIGHTGREEN);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r101);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler102(MouseEvent event) {
	    	
	    	if(pozycjaPionka102<0)pa=0;
	    
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka102,rk);
	    	if(pa==pozycjaPionka101||pa==pozycjaPionka103||pa==pozycjaPionka104)r102.setDisable(true);
	    	else{
			  r.setTranslateX(tabGreen[0][pa]);
			  r.setTranslateY(tabGreen[1][pa]);
			  r.setStroke(Color.LIGHTGREEN);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r102);
	    	}

	    }

	    @FXML
	    void onMouseCirrcler103(MouseEvent event) {
	    	
	    	if(pozycjaPionka103<0)pa=0;
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka103,rk);
	    	if(pa==pozycjaPionka101||pa==pozycjaPionka102||pa==pozycjaPionka104)r103.setDisable(true);
	    	else{
			  r.setTranslateX(tabGreen[0][pa]);
			  r.setTranslateY(tabGreen[1][pa]);
			  r.setStroke(Color.LIGHTGREEN);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r103);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler104(MouseEvent event) {
	    	
	    	if(pozycjaPionka104<0)
	    		pa=0;
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka104,rk);
	    	if(pa==pozycjaPionka101||pa==pozycjaPionka102||pa==pozycjaPionka103)r104.setDisable(true);
	    	else{
			  r.setTranslateX(tabGreen[0][pa]);
			  r.setTranslateY(tabGreen[1][pa]);
			  r.setStroke(Color.LIGHTGREEN);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r104);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler201(MouseEvent event) {
	    	if(pozycjaPionka201<0)
	    		pa=0;
	    	
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka201,rk);
	    	if(pa==pozycjaPionka202||pa==pozycjaPionka203||pa==pozycjaPionka204)r201.setDisable(true);
	    	else{
			  r.setTranslateX(tabBlue[0][pa]);
			  r.setTranslateY(tabBlue[1][pa]);
			  r.setStroke(Color.DODGERBLUE);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r201);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler202(MouseEvent event) {
	    	if(pozycjaPionka202<0)pa=0;
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka202,rk);
	    	if(pa==pozycjaPionka201||pa==pozycjaPionka203||pa==pozycjaPionka204)r202.setDisable(true);
	    	else{
			  r.setTranslateX(tabBlue[0][pa]);
			  r.setTranslateY(tabBlue[1][pa]);
			  r.setStroke(Color.DODGERBLUE);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r202);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler203(MouseEvent event) {
	    	if(pozycjaPionka203<0)pa=0;
	    	
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka203,rk);
	    	if(pa==pozycjaPionka201||pa==pozycjaPionka202||pa==pozycjaPionka204)r203.setDisable(true);
	    	else{
			  r.setTranslateX(tabBlue[0][pa]);
			  r.setTranslateY(tabBlue[1][pa]);
			  r.setStroke(Color.DODGERBLUE);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r203);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler204(MouseEvent event) {
	    	if(pozycjaPionka204<0)
	    		pa=0;
	    	
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka204,rk);
	    	if(pa==pozycjaPionka201||pa==pozycjaPionka202||pa==pozycjaPionka203)r204.setDisable(true);
	    	else{ 
	    	r.setTranslateX(tabBlue[0][pa]);
			  r.setTranslateY(tabBlue[1][pa]);
			  r.setStroke(Color.DODGERBLUE);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r204);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler301(MouseEvent event) {
	    	if(pozycjaPionka301<0)pa=0;
	    	
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka301,rk);
	    	if(pa==pozycjaPionka302||pa==pozycjaPionka303||pa==pozycjaPionka304)r301.setDisable(true);
	    	else{
			  r.setTranslateX(tabRed[0][pa]);
			  r.setTranslateY(tabRed[1][pa]);
			  r.setStroke(Color.RED);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r301);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler302(MouseEvent event) {
	    	if(pozycjaPionka302<0)pa=0;
	    	
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka302,rk);
	    	if(pa==pozycjaPionka301||pa==pozycjaPionka303|pa==pozycjaPionka304)r302.setDisable(true);
	    	else{ 
	    	r.setTranslateX(tabRed[0][pa]);
			  r.setTranslateY(tabRed[1][pa]);
			  r.setStroke(Color.RED);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r302);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler303(MouseEvent event) {
	    	if(pozycjaPionka303<0)pa=0;
	    	
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka303,rk);
	    	if(pa==pozycjaPionka301||pa==pozycjaPionka302|pa==pozycjaPionka304)r303.setDisable(true);
	    	else{
			  r.setTranslateX(tabRed[0][pa]);
			  r.setTranslateY(tabRed[1][pa]);
			  r.setStroke(Color.RED);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r303);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler304(MouseEvent event) {
	    	if(pozycjaPionka304<0)pa=0;
	    	
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka304,rk);
	    	if(pa==pozycjaPionka301||pa==pozycjaPionka302|pa==pozycjaPionka303)r304.setDisable(true);
	    	else{
			  r.setTranslateX(tabRed[0][pa]);
			  r.setTranslateY(tabRed[1][pa]);
			  r.setStroke(Color.RED);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r304);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler401(MouseEvent event) {
	    	if(pozycjaPionka401<0)pa=0;
	    	
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka401,rk);
	    	if(pa==pozycjaPionka402||pa==pozycjaPionka403|pa==pozycjaPionka404)r401.setDisable(true);
	    	else{
			  r.setTranslateX(tabYellow[0][pa]);
			  r.setTranslateY(tabYellow[1][pa]);
			  r.setStroke(Color.YELLOW);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r401);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler402(MouseEvent event) {
	    	if(pozycjaPionka402<0)pa=0;
	    	
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka402,rk);
	    	if(pa==pozycjaPionka401||pa==pozycjaPionka403|pa==pozycjaPionka404)r402.setDisable(true);
	    	else{
			  r.setTranslateX(tabYellow[0][pa]);
			  r.setTranslateY(tabYellow[1][pa]);
			  r.setStroke(Color.YELLOW);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r402);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler403(MouseEvent event) {
	    	if(pozycjaPionka403<0)pa=0;
	    	
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka403,rk);
	    	if(pa==pozycjaPionka401||pa==pozycjaPionka402|pa==pozycjaPionka404)r403.setDisable(true);
	    	else{
			  r.setTranslateX(tabYellow[0][pa]);
			  r.setTranslateY(tabYellow[1][pa]);
			  r.setStroke(Color.YELLOW);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r403);
	    	}
	    }

	    @FXML
	    void onMouseCirrcler404(MouseEvent event) {
	    	if(pozycjaPionka404<0)pa=0;
	    	
			  else pa = ruchpionka.obliczRuchPionka(pozycjaPionka404,rk);
	    	if(pa==pozycjaPionka401||pa==pozycjaPionka402|pa==pozycjaPionka403)r404.setDisable(true);
	    	else{
			  r.setTranslateX(tabYellow[0][pa]);
			  r.setTranslateY(tabYellow[1][pa]);
			  r.setStroke(Color.YELLOW);
			  r.setVisible(true);
			  r.toFront();
			  wykryjBicie(r404);
	    	}
	    }

	    @FXML
	    void outMouseCircler101(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" wybierz pionek");
	    	else label01.setText(nazwaGraczNiebieski.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler102(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" wybierz pionek");
	    	else label01.setText(nazwaGraczNiebieski.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler103(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" wybierz pionek");
	    	else label01.setText(nazwaGraczNiebieski.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler104(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" wybierz pionek");
	    	else label01.setText(nazwaGraczNiebieski.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler201(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==1)label01.setText(nazwaGraczNiebieski.getText()+" wybierz pionek");
	    	else if(gra.gracz==2)label01.setText(nazwaGraczCzerwony.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler202(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==1)label01.setText(nazwaGraczNiebieski.getText()+" wybierz pionek");
	    	else if(gra.gracz==2)label01.setText(nazwaGraczCzerwony.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler203(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==1)label01.setText(nazwaGraczNiebieski.getText()+" wybierz pionek");
	    	else if(gra.gracz==2)label01.setText(nazwaGraczCzerwony.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }
	    @FXML
	    void outMouseCircler204(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==1)label01.setText(nazwaGraczNiebieski.getText()+" wybierz pionek");
	    	else if(gra.gracz==2)label01.setText(nazwaGraczCzerwony.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler301(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==2)label01.setText(nazwaGraczCzerwony.getText()+" wybierz pionek");
	    	else if(gra.gracz==3)label01.setText(nazwaGraczZolty.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler302(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==2)label01.setText(nazwaGraczCzerwony.getText()+" wybierz pionek");
	    	else if(gra.gracz==3)label01.setText(nazwaGraczZolty.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler303(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==2)label01.setText(nazwaGraczCzerwony.getText()+" wybierz pionek");
	    	else if(gra.gracz==3)label01.setText(nazwaGraczZolty.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler304(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==2)label01.setText(nazwaGraczCzerwony.getText()+" wybierz pionek");
	    	else if(gra.gracz==3)label01.setText(nazwaGraczZolty.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	if(gra.gracz==2)label01.setText(nazwaGraczCzerwony.getText()+" wybierz pionek");
	    	else if(gra.gracz==3)label01.setText(nazwaGraczZolty.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler401(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==3)label01.setText(nazwaGraczZolty.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler402(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==3)label01.setText(nazwaGraczZolty.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler403(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==3)label01.setText(nazwaGraczZolty.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }

	    @FXML
	    void outMouseCircler404(MouseEvent event) {
	    	r.setVisible(false);
	    	bitypionekHologram.setTranslateX(277);
	    	if(gra.gracz==3)label01.setText(nazwaGraczZolty.getText()+" rzuc kostka");
	    	else if(gra.gracz==0)label01.setText(nazwaGraczZielony.getText()+" rzuc kostka");
	    	jestesZwyciezca();
	    }
		
	    public void blokujZielone(){
			r101.setDisable(true);
			r102.setDisable(true);
			r103.setDisable(true);
			r104.setDisable(true);
		}
		
		public void blokujNiebieski(){
			r201.setDisable(true);
			r202.setDisable(true);
			r203.setDisable(true);
			r204.setDisable(true);
		}
		
		public void blokujCzerwony(){
			r301.setDisable(true);
			r302.setDisable(true);
			r303.setDisable(true);
			r304.setDisable(true);
		}
		
		public void blokujZolty(){
			r401.setDisable(true);
			r402.setDisable(true);
			r403.setDisable(true);
			r404.setDisable(true);
		}
		
		public void blokujWszystko(){
			blokujZielone();
			blokujNiebieski();
			blokujCzerwony();
			blokujZolty();
		}
		
		public void odblokujZielone(){
			r101.setDisable(false);
			r102.setDisable(false);
			r103.setDisable(false);
			r104.setDisable(false);
		}
		
		public void odblokujNiebieski(){
			r201.setDisable(false);
			r202.setDisable(false);
			r203.setDisable(false);
			r204.setDisable(false);
		}
		
		public void odblokujCzerwony(){
			r301.setDisable(false);
			r302.setDisable(false);
			r303.setDisable(false);
			r304.setDisable(false);
		}
		
		public void odblokujZolty(){
			r401.setDisable(false);
			r402.setDisable(false);
			r403.setDisable(false);
			r404.setDisable(false);
		}
		
		public void odblokujWszystko(){
			odblokujZielone();
			odblokujNiebieski();
			odblokujCzerwony();
			odblokujZolty();
		}
		
		public void odblokujPrzycisk(){
			przycisk.setDisable(false);
			if(rk!=6){
			gra.kolejnyGracz();
			zmienPodswietlenie();
			}
			else{
				if(gra.gracz==0)odblokujZielone();
				else if(gra.gracz==1)odblokujNiebieski();
				else if(gra.gracz==2)odblokujCzerwony();
				else if(gra.gracz==3)odblokujZolty();
			}
		}
		
		@FXML
		public void wyborIlosciGraczy(){
			
			gra.setIloscGraczy(Integer.valueOf(iloscGraczyBox.getSelectionModel().getSelectedItem().toString()));
			iloscGraczyBox.toBack();
			nazwaGraczZielony.setDisable(false);
			
			if(gra.getIloscGraczy()>=2){ 
				nazwaGraczNiebieski.setDisable(false);
				widzialnePionkiZielone(true);
				widzialnePionkiNiebieski(true);
			}
			
			if (gra.getIloscGraczy()>=3){ 
				nazwaGraczCzerwony.setDisable(false);
				widzialnePionkiCzerwone(true);
			}
			
			if (gra.getIloscGraczy()>=4){
				nazwaGraczZolty.setDisable(false);
				widzialnePionkiZolte(true);
			}
			iloscGraczyBox.setDisable(true);
					 
		}
		
		@SuppressWarnings("rawtypes")
		@FXML
		private ComboBox iloscGraczyBox;
		
		@FXML
		private TextField nazwaGraczZielony;	
		@FXML
		public void wprowadzenieNazwyZielonego(){
			iloscWprowadzonychNazw++;
			if(iloscWprowadzonychNazw==gra.getIloscGraczy())przycisk.setDisable(false);
			nazwaGraczZielony.setDisable(true);
		}
		
		@FXML
		private TextField nazwaGraczNiebieski;
		@FXML
		public void wprowadzenieNazwyNiebieskiego(){
			iloscWprowadzonychNazw++;
			if(iloscWprowadzonychNazw==gra.getIloscGraczy())przycisk.setDisable(false);
			nazwaGraczNiebieski.setDisable(true);
		}
		
		@FXML
		private TextField nazwaGraczCzerwony;
		@FXML
		public void wprowadzenieNazwyCzerwonego(){
			iloscWprowadzonychNazw++;
			if(iloscWprowadzonychNazw==gra.getIloscGraczy())przycisk.setDisable(false);
			nazwaGraczCzerwony.setDisable(true);
		}
		
		@FXML
		private TextField nazwaGraczZolty;
		@FXML
		public void wprowadzenieNazwyZoltego(){
			iloscWprowadzonychNazw++;
			if(iloscWprowadzonychNazw==gra.getIloscGraczy())przycisk.setDisable(false);
			nazwaGraczZolty.setDisable(true);
		}

		public void widzialnePionkiZielone(Boolean widzialne){
			r101.setVisible(widzialne);
			r102.setVisible(widzialne);
			r103.setVisible(widzialne);
			r104.setVisible(widzialne);
		}
		
		public void widzialnePionkiNiebieski(Boolean widzialne){
			r201.setVisible(widzialne);
			r202.setVisible(widzialne);
			r203.setVisible(widzialne);
			r204.setVisible(widzialne);
		}
		
		public void widzialnePionkiCzerwone(Boolean widzialne){
			r301.setVisible(widzialne);
			r302.setVisible(widzialne);
			r303.setVisible(widzialne);
			r304.setVisible(widzialne);
		}
		
		public void widzialnePionkiZolte(Boolean widzialne){
			r401.setVisible(widzialne);
			r402.setVisible(widzialne);
			r403.setVisible(widzialne);
			r404.setVisible(widzialne);
		}
			
		public void zmianaKoloruPola(TextField kolejny, TextField poprzedni){
			kolejny.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
			poprzedni.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
		}

		public void zmienPodswietlenie(){
			
			switch (gra.getIloscGraczy()) {	
			case 2:  if(gra.gracz == 0) zmianaKoloruPola(nazwaGraczZielony, nazwaGraczNiebieski);
						else if(gra.gracz == 1) zmianaKoloruPola(nazwaGraczNiebieski, nazwaGraczZielony);
					break;
			
			case 3:  if(gra.gracz == 0) zmianaKoloruPola(nazwaGraczZielony, nazwaGraczCzerwony);
						else if(gra.gracz == 1) zmianaKoloruPola(nazwaGraczNiebieski, nazwaGraczZielony);
							else if(gra.gracz == 2) zmianaKoloruPola(nazwaGraczCzerwony, nazwaGraczNiebieski);
					break;
			
            case 4:  if(gra.gracz == 0) zmianaKoloruPola(nazwaGraczZielony, nazwaGraczZolty);
						else if(gra.gracz == 1) zmianaKoloruPola(nazwaGraczNiebieski, nazwaGraczZielony);
							else if(gra.gracz == 2) zmianaKoloruPola(nazwaGraczCzerwony, nazwaGraczNiebieski);
								else if(gra.gracz == 3)	zmianaKoloruPola(nazwaGraczZolty, nazwaGraczCzerwony);
            		break;
			}
			
		}
		
		public void wypiszNaLable02(String string){
			label02.setText(string);
			label02.setTextAlignment(TextAlignment.CENTER);
		}
		
		
		
		public void mozliwoscRuchu(){
			licznikBlokad=0;
			if(gra.gracz==0){
				if(rk!=1&&rk!=6&&pozycjaPionka101<0||pozycjaPionka101>=0&&(pozycjaPionka101==pozycjaPionka102+rk||pozycjaPionka101==pozycjaPionka103+rk||pozycjaPionka101==pozycjaPionka104+rk)){
					r101.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka102<0||pozycjaPionka102>=0&&(pozycjaPionka102==pozycjaPionka101+rk||pozycjaPionka102==pozycjaPionka103+rk||pozycjaPionka102==pozycjaPionka104+rk)){
					r102.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka103<0||pozycjaPionka103>=0&&(pozycjaPionka103==pozycjaPionka101+rk||pozycjaPionka103==pozycjaPionka102+rk||pozycjaPionka103==pozycjaPionka104+rk)){
					r103.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka104<0||pozycjaPionka104>=0&&(pozycjaPionka104==pozycjaPionka101+rk||pozycjaPionka104==pozycjaPionka102+rk||pozycjaPionka104==pozycjaPionka103+rk)){
					r104.setDisable(true);
					licznikBlokad++;
				}
			}
			else if(gra.gracz==1){
				if(rk!=1&&rk!=6&&pozycjaPionka201<0||pozycjaPionka201>=0&&(pozycjaPionka201==pozycjaPionka202+rk||pozycjaPionka201==pozycjaPionka203+rk||pozycjaPionka201==pozycjaPionka204+rk)){
					r201.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka202<0||pozycjaPionka202>=0&&(pozycjaPionka202==pozycjaPionka201+rk||pozycjaPionka202==pozycjaPionka203+rk||pozycjaPionka202==pozycjaPionka204+rk)){
					r202.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka203<0||pozycjaPionka203>=0&&(pozycjaPionka203==pozycjaPionka201+rk||pozycjaPionka203==pozycjaPionka202+rk||pozycjaPionka203==pozycjaPionka204+rk)){
					r203.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka204<0||pozycjaPionka204>=0&&(pozycjaPionka204==pozycjaPionka201+rk||pozycjaPionka204==pozycjaPionka202+rk||pozycjaPionka204==pozycjaPionka203+rk)){
					r204.setDisable(true);
					licznikBlokad++;
				}
			}
			else if(gra.gracz==2){
				if(rk!=1&&rk!=6&&pozycjaPionka301<0||pozycjaPionka301>=0&&(pozycjaPionka301==pozycjaPionka302+rk||pozycjaPionka301==pozycjaPionka303+rk||pozycjaPionka301==pozycjaPionka304+rk)){
					r301.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka302<0||pozycjaPionka302>=0&&(pozycjaPionka302==pozycjaPionka301+rk||pozycjaPionka302==pozycjaPionka303+rk||pozycjaPionka302==pozycjaPionka304+rk)){
					r302.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka303<0||pozycjaPionka303>=0&&(pozycjaPionka303==pozycjaPionka301+rk||pozycjaPionka303==pozycjaPionka302+rk||pozycjaPionka303==pozycjaPionka304+rk)){
					r303.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka304<0||pozycjaPionka304>=0&&(pozycjaPionka304==pozycjaPionka301+rk||pozycjaPionka304==pozycjaPionka302+rk||pozycjaPionka304==pozycjaPionka303+rk)){
					r304.setDisable(true);
					licznikBlokad++;
				}
			}
			else if(gra.gracz==3){
				if(rk!=1&&rk!=6&&pozycjaPionka401<0||pozycjaPionka401>=0&&(pozycjaPionka401==pozycjaPionka402+rk||pozycjaPionka401==pozycjaPionka403+rk||pozycjaPionka401==pozycjaPionka404+rk)){
					r401.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka402<0||pozycjaPionka402>=0&&(pozycjaPionka402==pozycjaPionka401+rk||pozycjaPionka402==pozycjaPionka403+rk||pozycjaPionka402==pozycjaPionka404+rk)){
					r402.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka403<0||pozycjaPionka403>=0&&(pozycjaPionka403==pozycjaPionka401+rk||pozycjaPionka403==pozycjaPionka402+rk||pozycjaPionka403==pozycjaPionka404+rk)){
					r403.setDisable(true);
					licznikBlokad++;
				}
				if(rk!=1&&rk!=6&&pozycjaPionka404<0||pozycjaPionka404>=0&&(pozycjaPionka404==pozycjaPionka401+rk||pozycjaPionka404==pozycjaPionka402+rk||pozycjaPionka404==pozycjaPionka403+rk)){
					r404.setDisable(true);
					licznikBlokad++;
				}
			}
			if(licznikBlokad>=4){
				label01.setText("brak mozliwosci ruchu, kliknij pomin aby zakonczyc ture");
				przycisk2.setDisable(false);
				przycisk2.toFront();
			}
		}
		
		
}
