package application;

import java.util.HashMap;
import java.util.Scanner;

public class ListaGraczy {
	
	private HashMap<Integer,Gracz> listaGraczy = new HashMap<Integer,Gracz>();
	private int iloscGraczy=0;
	
	public ListaGraczy(){
		
		System.out.print("Podaj Liczb� graczy: ");
		Scanner scanner = new Scanner(System.in);
		setIloscGraczy(scanner.nextInt());
		int numer = 0;
		while(listaGraczy.size()<iloscGraczy)
			listaGraczy.put(numer++, new Gracz());
		
		wypiszListeGraczy();
		
	}
	
	public void wypiszListeGraczy(){
		for(Gracz g : listaGraczy.values()){
			System.out.println(g.getNazwa());
		}
			
	}

	public int getIloscGraczy() {
		return iloscGraczy;
	}

	public void setIloscGraczy(int iloscGraczy) {
			this.iloscGraczy = iloscGraczy;
		
	}
	
	

}
